-module(boss_db_plugin_prv).

-export([init/1, 
		 do/1, 
		 format_error/1]).

-define(PROVIDER, boss_db_plugin).
-define(DEPS, [app_discovery]).

%% ===================================================================
%% Public API
%% ===================================================================

-spec init(rebar_state:t()) -> {ok, rebar_state:t()}.
init(State) ->
    Provider = providers:create([
            {name, ?PROVIDER},            % The 'user friendly' name of the task
            {module, ?MODULE},            % The module implementation of the task
            {bare, true},                 % The task can be run by the user, always true
            {deps, ?DEPS},                % The list of dependencies
            {example, "rebar3 boss_db_plugin"}, % How to use the plugin
            {opts, []},                   % list of options understood by the plugin
            {short_desc, "A boss_db rebar3 wrapper plugin."},
            {desc, "A boss_db rebar3 wrapper plugin."}
    ]),
    {ok, rebar_state:add_provider(State, Provider)}.


-spec do(rebar_state:t()) -> {ok, rebar_state:t()} | {error, string()}.
do(State) ->
	run_boss_db(rebar_state:current_app(State)),
    {ok, State}.

-spec format_error(any()) ->  iolist().
format_error(Reason) ->
    io_lib:format("~p", [Reason]).

%% ===================================================================
%% Internal Functions
%% ===================================================================

option_default(model_dir) -> "src/model";
option_default(out_dir)  -> "ebin";
option_default(source_ext) -> ".erl";
option_default(recursive) -> true;
option_default(compiler_options) -> [verbose, return_errors].

option(Opt, BossDbOpts) ->
    proplists:get_value(Opt, BossDbOpts, option_default(Opt)).

%% @doc Gets the boss_db options
boss_db_opts(App) ->
    rebar_app_info:get(App, boss_db_opts).

%% @doc A pre-compile hook to compile boss_db models
pre_compile_helper(RebarConf, BossDbOpts, TargetDir) ->
    SourceDir = option(model_dir, BossDbOpts),
    SourceExt = option(source_ext, BossDbOpts),
    TargetExt = ".beam",
    rebar_base_compiler:run(RebarConf, [],
        SourceDir,
        SourceExt,
        TargetDir,
        TargetExt,
        fun(S, T, _C) ->
            compile_model(S, T, BossDbOpts, RebarConf)
        end,
        [{check_last_mod, true},
        {recursive, option(recursive, BossDbOpts)}]).

set_debug_info_option(true, BossCompilerOptions) ->
    [debug_info | BossCompilerOptions];
set_debug_info_option(undefined, BossCompilerOptions) ->
    BossCompilerOptions.

compiler_options(ErlOpts, BossDbOpts) ->
    set_debug_info_option(proplists:get_value(debug_info, ErlOpts), 
						  option(compiler_options, BossDbOpts)).

compile_model(Source, Target, BossDbOpts, App) ->
    ErlOpts = rebar_app_info:get(App, erl_opts, []),
    RecordCompilerOpts = [{out_dir, filename:dirname(Target)}, 
						  {compiler_options, compiler_options(ErlOpts, BossDbOpts)}],
    case boss_record_compiler:compile(Source, RecordCompilerOpts) of
        {ok, _Mod} ->
            ok;
        {ok, _Mod, Ws} ->
            rebar_base_compiler:ok_tuple(Source, Ws);
        {error, Es, Ws} ->
            rebar_base_compiler:error_tuple(Source,
                                            Es, Ws, RecordCompilerOpts)
    end.

run_boss_db(App) ->
	RebarConf = rebar_app_info:opts(App),
	BossDbOpts = boss_db_opts(App),
	pre_compile_helper(RebarConf, BossDbOpts, option(out_dir, BossDbOpts)).
	