-module(boss_db_plugin).

-export([init/1]).

-spec init(rebar_state:t()) -> {ok, rebar_state:t()}.
init(State) ->
    boss_db_plugin_prv:init(State).