boss_db_plugin
=====

A boss_db rebar3 wrapper plugin.

Build
-----

    $ rebar3 compile

Use
---

Add the plugin to your rebar config:

    {plugins, [
        { boss_db_plugin, ".*", {git, "git@host:user/boss_db_plugin.git", {tag, "0.1.0"}}}
    ]}.

Then just call your plugin directly in an existing application:


    $ rebar3 boss_db_plugin
    ===> Fetching boss_db_plugin
    ===> Compiling boss_db_plugin
    <Plugin Output>
